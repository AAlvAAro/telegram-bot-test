FactoryBot.define do
  factory :message do
    id           { 1 }
    chat_id      { 12345 }
    text         { "MyString" }
    from_chatbot { false }
  end
end
