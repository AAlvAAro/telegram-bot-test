require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe 'GET #index' do
    it 'returns all chats' do
      allow(::Telegram::StoreMessagesService).to receive(:call) { nil }

      get :index
      expect(response.status).to eq(200)
    end
  end
end
