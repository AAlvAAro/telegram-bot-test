require 'rails_helper'

RSpec.describe ChatsController, type: :controller do
  let(:message) { FactoryBot.create(:message) }

  describe 'GET #show' do
    it 'returns a success response' do
      allow(::Telegram::StoreMessagesService).to receive(:call) { nil }

      get :show, params: { id: message.id }
      expect(response.status).to eq(200)
    end
  end
end
