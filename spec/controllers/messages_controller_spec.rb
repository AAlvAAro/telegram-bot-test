require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  let(:message) { FactoryBot.build(:message) }
  let(:message_params) {}

  describe 'POST #create' do
    context 'when the message is created @ Telegram' do
      it 'creates a new message' do
        allow_any_instance_of(Telegram::Gateway).to receive(:send_message)
          .with(message.chat_id, message.text) { true }

        post :create, params: message.attributes.slice('id', 'chat_id', 'text')

        new_message = Message.last
        expect(new_message.chat_id).to eq(message.chat_id)
        expect(new_message.text).to eq(message.text)
        expect(response.status).to eq(302)
      end
    end
  end
end
