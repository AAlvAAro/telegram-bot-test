require 'rails_helper'

module Telegram
  describe MessageBuilder do
    let(:response_object) do
      {
        "message_id": 8,
        "from": {
          "id": 273563309,
          "is_bot": false,
          "first_name": "Alvaro",
          "username": "AAlvAAro",
          "language_code": "en"
        },
        "chat": {
          "id": 273563309,
          "first_name": "Alvaro",
          "username": "AAlvAAro",
          "type": "private"
        },
        "date": 1618805875,
        "text": "/hello",
        "entities": [
          {
            "offset": 0,
            "length": 6,
            "type": "bot_command"
          }
        ]
      }.with_indifferent_access
    end


    describe '.build' do
      it 'returns a message ostruct object with the required attributes' do
        message = described_class.build(response_object)

        expect(message.id).to eq(8)
        expect(message.chat_id).to eq(273563309)
        expect(message.text).to eq('/hello')
      end
    end
  end
end
