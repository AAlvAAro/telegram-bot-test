require 'rails_helper'

module Telegram
  xdescribe Gateway do
    describe '#get_updates' do
      it "fetches all bot's messages from the API" do
        VCR.use_cassette('get_updates') do
          response = subject.get_updates
        end
      end
    end

    describe '#send_message' do
      it "sends a message to the API" do
        VCR.use_cassette('send_message') do
          response = subject.send_message(273563309, 'hello_world')
        end
      end
    end
  end
end
