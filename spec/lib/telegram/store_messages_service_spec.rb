require 'rails_helper'

module Telegram
  describe StoreMessagesService do
    let(:messages_updates) do
      [
        {
          "update_id": 542936237,
          "message": {
            "message_id": 100000,
            "from": {
              "id": 273563309,
              "is_bot": false,
              "first_name": "Alvaro",
              "username": "AAlvAAro",
              "language_code": "en"
            },
            "chat": {
              "id": 273563309,
              "first_name": "Alvaro",
              "username": "AAlvAAro",
              "type": "private"
            },
            "date": 1618805875,
            "text": "/hello",
            "entities": [
              {
                "offset": 0,
                "length": 6,
                "type": "bot_command"
              }
            ]
          }
        }
      ]
    end

    describe '.call' do
      it 'calls the API gateway to retrieve the messages and stores them' do
        allow_any_instance_of(Gateway).to receive(:get_updates) { messages_updates }

        described_class.call
        stored_message = Message.find(100000)
        expect(stored_message).to be_present
      end
    end
  end
end

