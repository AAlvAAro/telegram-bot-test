FROM ruby:2.7.3

# Base dependencies
RUN apt-get update
RUN apt-get install -fyq build-essential libpq-dev git

# Node
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -fyq nodejs
RUN npm i -g eslint

# Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn

# Cleanup
RUN apt-get autoremove -yq \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV app /app

RUN mkdir $app
WORKDIR $app
COPY Gemfile Gemfile.lock ./
RUN bundle check || bundle install
RUN yarn install
COPY package.json yarn.lock ./
RUN rails webpacker:install
RUN yarn install --check-files
COPY . ./
