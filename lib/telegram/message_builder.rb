require 'ostruct'

module Telegram
  class MessageBuilder
    def self.build(message)
      attributes = {
        id: message[:message_id],
        chat_id: message[:chat][:id],
        text: message[:text]
      }

      OpenStruct.new(attributes)
    end
  end
end
