module Telegram
  class Gateway
    include HTTParty

    base_uri "https://api.telegram.org/bot#{ENV['BOT_API_KEY']}"

    def get_updates
      response = self.class.get('/getUpdates')
      JSON.parse(response.body).with_indifferent_access[:result]
    end

    def send_message(chat_id, text)
      data = {
        chat_id: chat_id,
        text: text
      }.to_json

      response = self.class.post(
        '/sendMessage', body:
        data,
        headers: { 'Content-Type': 'application/json' }
      )

      message_response = JSON.parse(response.body).with_indifferent_access

      if message_response[:ok]
        message = Message.create(
          id: message_response[:message_id],
          chat_id: message_response[:chat_id],
          text: message_response[:text]
        )

        true
      else
        false
      end
    end
  end
end
