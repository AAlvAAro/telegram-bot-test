module Telegram
  class StoreMessagesService
    def self.call
      updates = Gateway.new.get_updates

      updates.each do |update|
        message = MessageBuilder.build(update[:message])

        Message.find_or_create_by(id: message.id) do |msg|
          msg.chat_id = message.chat_id
          msg.text = message.text
        end
      end
    end
  end
end
