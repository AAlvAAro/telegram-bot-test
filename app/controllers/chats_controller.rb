class ChatsController < ApplicationController
  def show
    Telegram::StoreMessagesService.call

    @chat_id = params[:id]
    @messages = Message.where(chat_id: @chat_id)
  end
end
