class MessagesController < ApplicationController
  def create
    chat_id = params[:chat_id].to_i
    text = params[:text]

    message_created = Telegram::Gateway.new.send_message(chat_id, text)


    if message_created
      message = Message.new(
        chat_id: chat_id,
        text: text,
        from_chatbot: true
      )
    else
      redirect_to chat_path(chat_id), error: 'There was an error trying to create the message'
    end

    if message.save
      redirect_to chat_path(chat_id), notice: 'Reply has been sent!'
    else
      redirect_to chat_path(chat_id), error: message.errors.full_messages.to_sentence
    end
  end
end
