class HomeController < ApplicationController
  def index
    Telegram::StoreMessagesService.call

    @chats = Message.group(:chat_id).count
  end
end
