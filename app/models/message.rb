class Message < ApplicationRecord
  validates_presence_of :chat_id, :text
end
