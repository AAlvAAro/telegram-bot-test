class CreateMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :messages do |t|
      t.integer :chat_id
      t.string :text
      t.boolean :from_chatbot, default: false

      t.timestamps
    end
  end
end
