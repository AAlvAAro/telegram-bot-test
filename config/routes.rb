Rails.application.routes.draw do
  get 'chats/index'
  get 'messages/create'
  root to: 'home#index'

  resources :chats, only: :show
  resources :messages, only: :create
end
