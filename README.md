## Bot setup

For running the project a Telegram Bot needs to be created and API_KEY. Once you have the key, copy the content on `config/application.yml.sample` into a `config/application.yml` file
and replace the API_KEY value with your key. Instructions for creating the bot can be found here:



## How to run the project

To start working on this codebase, do the following:

Project setup:
  `docker-compose build`
  `docker-compose run --rm app bundle`
  `docker-compose run --rm app rake db:create && db:migrate`
  `docker-compose up`

The UI consists of:

- Index, will show every chat / conversation for that bot
- A link on the index page would take you to the chat view, where you can reply to messages and see the existent ones

## Running tests

Tests can be run with the following command

  `docker-compose run --rm app rspec`

## Heroku app

The web app demo will be available at https://telegram-api-demo.herokuapp.com

## Things to improve

Some of the things I would like to improve if I had more time are:

- Gateway spec is skipped, I'm having an issue with the import of VCR and webmock
- Add a Chat model and store chats to track the users that reach out and associate messages to that table
- Add additional containers for test app and test database to run them separately on the specs run
- Run the service that fetches the messages though a sidekiq worker
- I can see an issue when storing a reply before new messages are pulled, that way the order of the messages would not
be correct. To fix that setting a webhook would help.
